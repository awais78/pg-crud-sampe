'use strict'
const pgconfig = require('../utils/configuration');
const pg = require('pg');
const utils = require('../utils/utils');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

module.exports.handler = (event, context, callback) => {
    if(event.httpMethod == "GET") {
         get_user(event, context, callback);
    } else if(event.httpMethod == "POST") {
         save_user(event, context, callback);
    } else if(event.httpMethod == "PUT") {
         update_user(event, context, callback);
    } else if(event.httpMethod == "DELETE") {
         delete_user(event, context, callback);
    } else {
        utils.respond(context, 400, {message: "Method does not exist."});
    }
}

const get_user = (event, context, callback) => {
    let where = {};
    if(event.pathParameters === null || event.pathParameters.id == undefined || event.pathParameters.id == "") {
        
    } else {
        where.id = event.pathParameters.id;
    }

    let q = utils.createGetQuery('users', where);

    let client = new pg.Client(pgconfig.connConfig);
    client.connect();
    client.query(q.query, q.params, function(err,result){
        if(err) {
          utils.respond(context, 500, {message: err.message});
        } else {
            if(result.rowCount > 0)
                utils.respond(context, 200, result.rows);
            else
                utils.respond(context, 200, {message: "No users exists against your query."});
        }
        client.end();
    });
}

const save_user = (event, context, callback) => {
    //let body = JSON.parse(event.body);
    let body = event.body;

    let client = new pg.Client(pgconfig.connConfig);
    client.connect();

    body.id = uuidv1();
    body.created_at = moment().format("YYYY-MM-DD HH:mm:ss");
    let q = utils.createInsertQuery('users', body);

    client.query(q.query, q.params, function(err,result){
        if(err) {
          utils.respond(context, 500, {message: err.message});
        } else {
            utils.respond(context, 200, {message: "User has been added successfully."});
        }
        client.end();
    });
}

const update_user = (event, context, callback) => {
    //let body = JSON.parse(event.body);
    let body = event.body;

    if(event.pathParameters === null || event.pathParameters.id == undefined || event.pathParameters.id == "") {
        utils.respond(context, 400, {message: "Mandatory parameters are missing."});
    } else {

        let where = {id: event.pathParameters.id};
        let q = utils.createUpdateQuery('users', body, where);

        let client = new pg.Client(pgconfig.connConfig);
        client.connect();
        client.query(q.query, q.params, function(err,result){
            if(err) {
              utils.respond(context, 500, {message: err.message});
            } else {
                if(result.rowCount > 0)
                    utils.respond(context, 200, {message: "User has been updated successfully."});
                else
                    utils.respond(context, 200, {message: "User ID does not exist."});
            }
            client.end();
        });
    }
}

const delete_user = (event, context, callback) => {
    if(event.pathParameters === null || event.pathParameters.id == undefined || event.pathParameters.id == "") {
        utils.respond(context, 400, {message: "Mandatory parameters are missing."});
    } else {
        let where = {id: event.pathParameters.id};
        let q = utils.createDeleteQuery('users', where);

        let client = new pg.Client(pgconfig.connConfig);
        client.connect();
        client.query(q.query, q.params, function(err,result){
            if(err) {
              utils.respond(context, 500, {message: err.message});
            } else {
                if(result.rowCount > 0)
                    utils.respond(context, 200, {message: "User has been deleted successfully."});
                else
                    utils.respond(context, 200, {message: "User ID does not exist."});
            }
            client.end();
        });
    }
}