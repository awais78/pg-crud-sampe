'use strict'

module.exports.respond = (context, statusCode, content) => {
	const response = {
        statusCode: statusCode,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify(content)
    };
    context.succeed(response);
}

module.exports.createInsertQuery = (tablename, obj) => {
    let insert = 'insert into ' + tablename;

    let keys = Object.keys(obj);
    let dollar = keys.map(function (item, idx) { return '$' + (idx + 1); });
    let values = Object.keys(obj).map(function (k) { return obj[k]; });
    return {
        query: insert + ' (' + keys + ')' + ' values (' + dollar + ')',
        params: values
    }
}

module.exports.createUpdateQuery = (tablename, obj, where) => {
    let update = 'update ' + tablename;
    let keys = Object.keys(obj);
    let c = 0;
    let dollar = keys.map(function (item, idx) {c = idx + 1; return '$' + (idx + 1); });
    let values = Object.keys(obj).map(function (k) { return obj[k]; });

    c++;
    let w = '';
	for(let i in where) {
		w += i + ' = $' + c;
		values.push(where[i]);
		c++;
	}    

    return {
        query: update + ' set (' + keys + ')' + ' = (' + dollar + ') where ' + w,
        params: values
    }
}

module.exports.createDeleteQuery = (tablename, where) => {
    let del = 'delete from ' + tablename;
    let c = 0;
    let values = [];

    c++;
    let w = '';
	for(let i in where) {
		w += i + ' = $' + c;
		values.push(where[i]);
		c++;
	}    

    return {
        query: del + ' where ' + w,
        params: values
    }
}

module.exports.createGetQuery = (tablename, where) => {
    let get = 'select * from ' + tablename;
    let c = 0;
    let values = [];

    c++;
    let w = '';
	for(let i in where) {
		w += i + ' = $' + c;
		values.push(where[i]);
		c++;
	}    

	if(w == '') {
		return {
	        query: get,
	        params: values
	    }
	} else {
		return {
	        query: get + ' where ' + w,
	        params: values
	    }
	}
}